const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();
const https = require('https');
const fs = require('fs');
const port = 3001;
const cacheControl = require('express-cache-controller');


var key = fs.readFileSync(__dirname + '/certs/selfsigned.key');
var cert = fs.readFileSync(__dirname + '/certs/selfsigned.crt');
var options = {
    key: key,
    cert: cert
};

app.disable('x-powered-by');
app.use(cacheControl({
    private: true,
    noCache: true,
    noStore: true,
    mustRevalidate: true,    
    }))
app.use(express.static(path.join(__dirname,'build')));
app.use(express.static('private', {
etag: false,
lastModified: true,
setHeaders: (res, path) => {
    if (path.endsWith('.html')) {
        res.setHeader('Cache-Control','no-cache');
            }
        }
      }
    ));

app.get('*', function(req, res)
{
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

var server = https.createServer(options, app);

server.listen(port, () => {
    console.log('Frontend start on port: ' + port )
});