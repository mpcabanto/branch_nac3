import { grayColor, infoColor, whiteColor, dangerColor } from "assets/jss/material-dashboard-react.jsx";

const cardFooterStyle = {
  cardFooter: {
    padding: "0",
    paddingTop: "10px",
    justifyContent: "space-between",
    alignItems: "center",
    display: "flex",
    backgroundColor: "transparent",
    border: "0"
  },
  cardFooterProfile: {
  padding: "0",
  paddingTop: "10px",
  margin: "0 15px 10px",
  borderRadius: "0",
  justifyContent: "center",  
  alignItems: "center",
  display: "flex",
  backgroundColor: "transparent",
  border: "0",
  color: infoColor[0]
    },
    cardFooterBlue: {
      marginTop: "20px",
      "& svg": {
        display: "flex",
        top: "4px",
        justifyContent: "center",  
        alignItems: "center",
        marginRight: "3px",
        marginLeft: "3px",
        width: "16px",
        height: "30px"},
        display: "flex",
        top: "4px",
        justifyContent: "center",  
        alignItems: "center",
        background:
          "linear-gradient(30deg, " + infoColor[0] + ", " + infoColor[1] + ")",
    color: whiteColor
      },
      cardFooterRed: {
        marginTop: "0px",
        "& svg": {
          display: "flex",
          top: "4px",
          justifyContent: "center",  
          alignItems: "center",
          marginRight: "3px",
          marginLeft: "3px",
          width: "16px",
          height: "30px"},
          display: "flex",
          top: "4px",
          justifyContent: "center",  
          alignItems: "center",
          background:
            "linear-gradient(30deg, " + dangerColor[0] + ", " + dangerColor[1] + ")",
      color: whiteColor
        },
  cardFooterPlain: {
    paddingLeft: "5px",
    paddingRight: "5px",
    backgroundColor: "transparent"
  },
  cardFooterStats: {
    borderTop: "1px solid " + grayColor[10],
    marginTop: "20px",
    "& svg": {
      position: "relative",
      top: "4px",
      marginRight: "3px",
      marginLeft: "3px",
      width: "16px",
      height: "16px"
    },
    "& .fab,& .fas,& .far,& .fal,& .material-icons": {
      fontSize: "16px",
      position: "relative",
      top: "4px",
      marginRight: "3px",
      marginLeft: "3px"
    }
  },
  cardFooterChart: {
    borderTop: "1px solid " + grayColor[10]
  }
};

export default cardFooterStyle;
