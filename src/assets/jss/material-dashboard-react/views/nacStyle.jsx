import {
    blackColor,
    whiteColor,
    hexToRgb
  } from "assets/jss/material-dashboard-react.jsx";
  
import { boxShadow as boxShadowx } from "assets/jss/material-dashboard-react.jsx";

  const nacStyle = {
    root: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    wrapper: {
      margin: "1",
      position: 'relative',
    },    
  buttonProgress: {
    color: 'info',
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
    formControl: {
      margin: "1px",
      minWidth: 120,
    },
    typo: {
      paddingLeft: "2%",
      marginBottom: "1px",
      fontWeight: "400",
      position: "relative"
    },
    note: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
      bottom: "10px",
      color: "#c0c1c2",
      display: "block",
      fontWeight: "400",
      fontSize: "11px",
      lineHeight: "13px",
      left: "0",
      marginLeft: "20px",
      position: "absolute",
      width: "260px"
    },
    cardCategoryWhite: {
      color: "#1635ff",
      margin: "0",
      fontSize: "12px",
      marginTop: "0",
      marginBottom: "0"
    },
    cardTitleBlue: {
      color: "#1635ff",
      marginTop: "0px",
      minHeight: "auto",
      fontWeight: "600",
      fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
      marginBottom: "3px",
      textTransform: "none",
      textDecoration: "none"
    },
    cardTitleBlack: {
      color: "#000000",
      marginTop: "0px",
      minHeight: "auto",
      fontWeight: "600",
      marginBottom: "3px",
      textTransform: "none",
      textDecoration: "none"
    },
    button: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
      verticalAlign: "middle",
      fontWeight: "600",
      lineHeight: "1.5em",
      textTransform: "none",
      textDecorationLine: "underline"
    },
    txtbutton: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
      fontWeight: "600",
      lineHeight: "1.5em",
      fontSize: "14px",
      textDecorationLine: "underline",
      textTransform: "none",
      justify: "left",
      alignItems: "left"
    },
    
    txtbuttonblue: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
      fontWeight: "600",
      lineHeight: "1.5em",
      fontSize: "14px",
      textDecorationLine: "underline",
      justify: "left",
      alignItems: "left",
      textTransform: "none",
      color: "#1635ff",
    },
    centertextblue: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
      fontWeight: "600",
      lineHeight: "1.5em",
      fontSize: "12px",
      justifyContent: "center",
      alignItems: "center",
      justify: "center",
      color: "#1635ff",
    },
    
    centeritems: {
      direction: "row",
      justify: "center",
      alignItems: "center",
      spacing: "3px"
    },
    paper: {
      position: 'absolute',
      width: 400,
      border: '2px solid #000',
      outline: 'none',
      background: "white",
      boxShadow: boxShadowx,
      padding: "1px"
    },
    input: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
      fontWeight: "600",
      fontSize: "14px",
      borderRadius: 4,
      position: 'relative',
      width: "100%"
    }  ,
    select: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
      fontWeight: "600",
      fontSize: "9px",
      borderRadius: 4,
      position: 'relative',
      width: "250px",
      height: "35px",
      marginTop: "7px",
      marginBottom: "7px",
      marginRight: "3px"
    }  ,
    boxedselect: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
      fontWeight: "600",
      fontSize: "12px",
      borderRadius: 4,
      position: 'relative',
      width: "250px",
      height: "35px",
      marginTop: "7px",
      marginBottom: "7px",
      marginRight: "3px"
    }  ,
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    }
  };
  
  export default nacStyle;
  