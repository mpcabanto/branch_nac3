/* eslint-disable */
import React from "react";
import PropTypes from "prop-types";
import { Switch, Route, Redirect } from "react-router-dom";
// creates a beautiful scrollbar
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Navbar from "components/Navbars/Navbar.jsx";
import Footer from "components/Footer/Footer.jsx";
import Sidebar from "components/Sidebar/Sidebar.jsx";
import FixedPlugin from "components/FixedPlugin/FixedPlugin.jsx";

import routes from "routes.js";

import dashboardStyle from "assets/jss/material-dashboard-react/layouts/dashboardStyle.jsx";

import image from "assets/img/BGText.jpg";
import logo from "assets/img/PSBmini.JPG";

import * as Keycloak from 'keycloak-js';
///Start of Keycloak Portion////

const switchRoutes = (
  <Switch>
    {routes.map((prop, key) => {
      if (prop.layout === "/admin") {
          return (
            <Route
              path={prop.layout + prop.path}
              component={prop.component}
              key={key}            
            />
          );
        
      }
    })}
  </Switch>
);

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      image: image,
      color: "blue",
      hasImage: true,
      fixedClasses: "dropdown show",
      mobileOpen: false
      , keycloak: null, authenticated: false
    };
  }
  handleImageClick = image => {
    this.setState({ image: image });
  };
  handleColorClick = color => {
    this.setState({ color: color });
  };
  handleFixedClick = () => {
    if (this.state.fixedClasses === "dropdown") {
      this.setState({ fixedClasses: "dropdown show" });
    } else {
      this.setState({ fixedClasses: "dropdown" });
    }
  };
  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
  };
  getRoute() {
    return this.props.location.pathname !== "/admin/maps";
  }
  resizeFunction = () => {
    if (window.innerWidth >= 960) {
      this.setState({ mobileOpen: false });
    }
  };
  componentDidMount() {
    const keycloak = Keycloak(`${process.env.PUBLIC_URL}/keycloak.json`    );

    keycloak.init({onLoad: 'login-required', promiseType:'native'}).then(authenticated => {

      this.setState({ keycloak: keycloak, authenticated: authenticated })

      if (!this.state.authenticated) {
        window.location.reload();
        clear.localStorage();
      } else {
        console.info("Authenticated");
        //Keycloak Token
        localStorage.setItem("token", keycloak.token)
        let roleNames = "";
        let groups = "";
        let clientId = keycloak.clientId;
        console.log(keycloak);
        keycloak.resourceAccess[clientId].roles.map(m => (
          roleNames += m + ","
        ))
        keycloak.tokenParsed.groups.map(m => (
          groups += m.replace("/RC","") + ","
        ))
        console.log(keycloak.tokenParsed.groups);
        //Listing Roles from assigned from keycloak
        localStorage.setItem("userName", keycloak.tokenParsed.name);
        localStorage.setItem("userId", keycloak.tokenParsed.preferred_username);
        localStorage.setItem("roleNames", roleNames.substr(0,roleNames.length - 1));
        localStorage.setItem("groups", groups.substr(0,groups.length - 1));
        if (navigator.platform.indexOf("Win") > -1) {
          const ps = new PerfectScrollbar(this.refs.mainPanel);
        }
        window.addEventListener("resize", this.resizeFunction);
      }
    })
  };

  componentDidUpdate(e) {
    if (e.history.location.pathname !== e.location.pathname) {
      this.refs.mainPanel.scrollTop = 0;
      if (this.state.mobileOpen) {
        this.setState({ mobileOpen: false });
      }
    }
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.resizeFunction);
  }
  render() {
    const { classes, ...rest } = this.props;

    return (
      <div className={classes.wrapper}>
        <Sidebar
          routes={routes}
          logoText={"New Accounts"}
          logo={logo}
          image={this.state.image}
          handleDrawerToggle={this.handleDrawerToggle}
          open={this.state.mobileOpen}
          color={this.state.color}
          {...rest}
        />
        <div className={classes.mainPanel} ref="mainPanel">
          <Navbar
            routes={routes}
            handleDrawerToggle={this.handleDrawerToggle}
            {...rest}
          />
         
            <div className={classes.content}>
              <div className={classes.container}>{switchRoutes}</div>
            </div>
          
           <Footer /> 

        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Dashboard);
