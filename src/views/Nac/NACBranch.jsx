import React from "react";
import axios from 'axios';
import withStyles from "@material-ui/core/styles/withStyles";
import moment from 'moment';

import PropTypes from "prop-types";
import nacStyle from "assets/jss/material-dashboard-react/views/nacStyle.jsx";

// core components
import Draggable, { DraggableCore } from 'react-draggable';
import "assets/css/modal.css";
import CameraModal from './CameraModal';
import NACModal from './NACModal';
import NACSubmitted from './NACSubmitted';

import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Grid from "@material-ui/core/Grid";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import Primary from "components/Typography/Primary.jsx";
import Paper from '@material-ui/core/Paper';

import PhotoCamera from '@material-ui/icons/PhotoCamera';
import Modal from '@material-ui/core/Modal';
import Buttonx from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Input from "@material-ui/core/Input";
import InputBase from '@material-ui/core/InputBase';
import Typography from '@material-ui/core/Typography';
//import { Datepicker } from '@material-ui/pickers';

import NacList from "views/Nac/NacList.jsx";
import Buttonb from "react-bootstrap/Button";
import { Route, NavLink, Link as RouterLink } from "react-router-dom";
import Link from '@material-ui/core/Link';
import LinearProgress from '@material-ui/core/LinearProgress';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FormControl from '@material-ui/core/FormControl';
import { Text, TextInputField, Pane, SelectField } from 'evergreen-ui';
import ComboBox from '@zippytech/react-toolkit/ComboBox';
import '@zippytech/react-toolkit/ComboBox/index.css';
import Collapse from '@material-ui/core/Collapse';
import CardContent from '@material-ui/core/CardContent';
import TypoWarning from "components/Typography/Warning.jsx";
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import clsx from 'clsx';

const AdapterLink = React.forwardRef((props, ref) => <RouterLink innerRef={ref} {...props} />);


class NACBranch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: "",
            fullname: "",
            isOpen: false,
            isConfirmed: false,
            data: {}, clientInfo: {}, iResult: {}, permAdd: {}, presentAdd: {}, application: {},
            acResult: [], dedupDesc: [], icdbDesc: [], carsDesc: [],
            municipalityList: {}, countriesList: {}, provinceList: {}, incomesourceList: {}, professionList: {},
            acImages: {},
            email: '',
            name: '',
            phone: '',
            dob: '',
            gender: 'M',
            spouseName: 'NA',
            civilStatus: 'Single',
            formErrors: { email: '', name: '', phone: '' },
            emailValid: false,
            nameValid: false,
            phoneValid: false,
            dobValid: false,
            formValid: false,
            isError: false,
            isModalOpen: false,
            mailexpanded : false,
            items: [
                { id: 0, desc: 'No Data', showModal: false }
            ],
            vRefNo: "",
            cameraOpen: false,
            vDocType: "01",
            labelWidth: 0,
            ranges: [
                {
                    value: 'FN1',
                    label: 'FILIPINO',
                },
                {
                    value: 'FN2',
                    label: 'FOREIGN',
                }
            ]
        };
    };


    componentDidMount() {
        if (!this.props.location.state) {
            this.getCustomerDetails()
        } else {

            this.setState({ clientInfo: this.props.location.state });
        }
        this.getReferenceAPI()

        if (this.state.clientInfo.statusCode == 50) {
            this.setState({isConfirmed: true})
        }
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevState.clientInfo !== this.state.clientInfo) {
            if (!this.state.clientInfo) {
                this.getDedup("0");
            } else {
                this.getDedup(this.state.clientInfo);
            }


        }
    }

    getCustomerDetails = async () => {

        const headers = { "headers": "Access-Control-Allow-Origin" }
        let rc = localStorage.getItem("groups");
        let vappno = this.state.vRefNo;
        // axios.post('http://localhost:11352/api/WebApi/GetKafka', { kafkaurl: "http://10.11.27.104:5003/api/NacReview", kafkadata: null })
         await axios.post('https://10.11.27.104:3001/api/WebApi/GetKafka', { kafkaurl: "http://10.11.27.104:5003/api/NacReview/3/" + rc , kafkadata: null })
       //  await axios.get('/json/customer3.json')
            .then(response => {
                let results = null;
                if (typeof (response.data) === "string") {
                    results = JSON.parse(response.data);
                } else {
                    results = response.data;
                }
                this.setState({ clientInfo: results });
                //console.log(this.state.errorResult);
                console.log(this.state.clientInfo);
                let vsourceofIncome = this.removeBar(this.state.clientInfo.sourceofIncome);
                let vnatureofWork = this.removeBar(this.state.clientInfo.natureofWork);
                let state = this.state;
                this.setState({
                    ...state,
                    clientInfo: { ...state.clientInfo, "sourceofIncome": vsourceofIncome, "natureofWork": vnatureofWork }
                });
            })
            .catch((ex) => {
                console.error(ex);
                this.setState({ isError: true });
                alert(ex);
            });


    }; //end getCustomerDetails

    getReferenceAPI = async () => {

        const headers = { "headers": "Access-Control-Allow-Origin" }
        await axios.all([
            axios.get('https://10.11.27.104:3001/api/WebApi/GetCountry', { headers }),
            //axios.post('https://10.11.27.104:3001/api/WebApi/GetApi', { kafkaurl: "https://psbapiadev:8243/country", kafkadata: null }),
            axios.post('https://10.11.27.104:3001/api/WebApi/GetApi', { kafkaurl: "https://psbapiadev:8243/country/PH/town", kafkadata: null }),
            axios.post('https://10.11.27.104:3001/api/WebApi/GetApi', { kafkaurl: "https://psbapiadev:8243/country/PH/province", kafkadata: null }),
            axios.get('https://10.11.27.104:3001/api/WebApi/GetIncomeSource', { headers }),
            axios.get('https://10.11.27.104:3001/api/WebApi/GetProfession', { headers }),
        ])
            .then(axios.spread((data1, data2, data3, data4, data5) => {
                this.setState({
                    countriesList: data1.data,
                    countrypres: "PH",
                    municipalityList: data2.data, //municipalityTempList: data2.data, municipalityTemp2List: data2.data,
                    provinceList: data3.data,
                    incomesourceList: data4.data,
                    professionList: data5.data
                })
                console.log(this.state);
            }
            ))
    }

    renderBirthPlace() {

        if (this.state.provinceList.data) {
            return (
                <form className={this.props.classes.container} >
                    <TextField
                        select
                        className={this.props.classes.select}
                        name="placeofBirth"
                        value={this.state.clientInfo.placeofBirth}
                        onChange={this.fieldOnChange.bind(this)}
                    >
                        {this.state.provinceList.data.map(option => (
                            <MenuItem key={option.code} value={option.code + "|" + option.name}>
                                {option.name}
                            </MenuItem>
                        ))}
                    </TextField>
                </form>
            )
        }
    }

    renderFinancial() {
        const { classes } = this.props;
        const { clientInfo } = this.state;
        if (this.state.provinceList.data) {
            return (
                <CardBody>
                    <Grid container alignItems="center">
                        <Grid item xs={12} sm={12} md={4}><Primary>Source of Funds: </Primary>
                        </Grid>
                        <Grid item xs={12} sm={12} md={7}>
                            <form className={classes.container} >
                                <TextField
                                    select
                                    className={classes.select}
                                    name="sourceofIncome"
                                    value={clientInfo.sourceofIncome}
                                    onChange={this.fieldOnChange.bind(this)}
                                >
                                    {this.state.incomesourceList.data.map(option => (
                                        <MenuItem key={option.code} value={option.code + "|" + option.description}>
                                            {option.description}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </form>
                        </Grid>
                    </Grid>
                    <Grid container alignItems="center">
                        <Grid item xs={12} sm={12} md={4}><Primary>Nature of Work: </Primary>
                        </Grid>
                        <Grid item xs={12} sm={12} md={7}>

                            <form className={classes.container} >
                                <TextField
                                    select
                                    className={classes.select}
                                    name="natureofWork"
                                    value={clientInfo.natureofWork}
                                    onChange={this.fieldOnChange.bind(this)}
                                >
                                    {this.state.professionList.data.map(option => (
                                        <MenuItem key={option.code} value={option.code + "|" + option.description}>
                                            {option.description}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </form>
                        </Grid>
                    </Grid>
                </CardBody>
            )
        }
    }
    handleChange = ({ option }) => {
        this.setState({
            selectedOption: option
        })
    }

    renderAddress() {
        const { classes } = this.props;

        const { clientInfo } = this.state;
        if (this.state.provinceList.data) {
            return (

                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="gray">
                            <h6 className={classes.cardTitleBlack}>Address:</h6>

                        </CardHeader>
                        <CardBody>
                            <Grid container>
                                <Grid item xs={12} sm={12} md={4}><Primary>Present Address: </Primary>
                                </Grid>
                                <Grid item xs={12} sm={12} md={12}>
                                    <form className={classes.container} >
                                        <TextField
                                            label="Address Line 1"
                                            id="simple-start-adornment"
                                            name="presentAddress"
                                            className={classes.select}
                                            value={clientInfo.presentAddress}
                                            onChange={this.fieldOnChange.bind(this)}
                                        />
                                        <TextField
                                            select
                                            label="Municipality"
                                            className={classes.select}
                                            name="presentMunicipalityCode"
                                            value={clientInfo.presentMunicipalityCode}
                                            onChange={this.fieldOnChange.bind(this)}
                                        >
                                            {this.state.municipalityList.data.map(option => (
                                                <MenuItem key={option.code} value={option.code + "|" + option.name}>
                                                    {option.name}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                        <TextField
                                            select
                                            label="Province"
                                            className={classes.select}
                                            name="presentProvince"
                                            value={clientInfo.presentProvince}
                                            onChange={this.fieldOnChange.bind(this)}
                                        >
                                            {this.state.provinceList.data.map(option => (
                                                <MenuItem key={option.code} value={option.code + "|" + option.name}>
                                                    {option.name}
                                                </MenuItem>
                                            ))}
                                        </TextField>

                                        <TextField
                                            select
                                            label="Country"
                                            className={classes.select}
                                            name="presentCountry"
                                            value={clientInfo.presentCountry}
                                            onChange={this.fieldOnChange.bind(this)}
                                        >
                                            {this.state.countriesList.data.map(option => (
                                                <MenuItem key={option.alpha2} value={option.alpha2 + "|" + option.name}>
                                                    {option.name}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                        <TextField
                                            label="Zipcode"
                                            id="simple-start-adornment"
                                            name="presentZipCode"
                                            className={classes.select}
                                            value={clientInfo.presentZipCode}
                                            onChange={this.fieldOnChange.bind(this)}
                                        />
                                    </form>
                                </Grid>
                            </Grid>
                        </CardBody>
                        <CardBody>
                            <Grid container>
                                <Grid item xs={12} sm={12} md={4}><Primary>Permanent Address: </Primary>
                                </Grid>
                                <Grid item xs={12} sm={12} md={12}>
                                    <form className={classes.container} >
                                        <TextField
                                            label="Address Line 1"
                                            id="simple-start-adornment"
                                            name="permanentAddress"
                                            className={classes.select}
                                            value={clientInfo.permanentAddress}
                                            onChange={this.fieldOnChange.bind(this)}
                                        />
                                        <TextField
                                            select
                                            label="Municipality"
                                            className={classes.select}
                                            name="permanentMunicipalityCode"
                                            value={clientInfo.permanentMunicipalityCode}
                                            onChange={this.fieldOnChange.bind(this)}
                                        >
                                            {this.state.municipalityList.data.map(option => (
                                                <MenuItem key={option.code} value={option.code + "|" + option.name}>
                                                    {option.name}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                        <TextField
                                            select
                                            label="Province"
                                            className={classes.select}
                                            name="permanentProvince"
                                            value={clientInfo.permanentProvince}
                                            onChange={this.fieldOnChange.bind(this)}
                                        >
                                            {this.state.provinceList.data.map(option => (
                                                <MenuItem key={option.code} value={option.code + "|" + option.name}>
                                                    {option.name}
                                                </MenuItem>
                                            ))}
                                        </TextField>

                                        <TextField
                                            select
                                            label="Country"
                                            className={classes.select}
                                            name="permanentCountry"
                                            value={clientInfo.permanentCountry}
                                            onChange={this.fieldOnChange.bind(this)}
                                        >
                                            {this.state.countriesList.data.map(option => (
                                                <MenuItem key={option.alpha2} value={option.alpha2 + "|" + option.name}>
                                                    {option.name}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                        <TextField
                                            label="Zipcode"
                                            id="simple-start-adornment"
                                            name="permanentZipCode"
                                            className={classes.select}
                                            value={clientInfo.permanentZipCode}
                                            onChange={this.fieldOnChange.bind(this)}
                                        />
                                    </form>
                                </Grid>
                            </Grid>
              <CardActions disableSpacing>
              <Primary>Mailing Address: </Primary>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: this.state.mailexpanded,
          })}
          onClick={this.handleExpandClick()}
          aria-expanded={this.state.mailexpanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>

      <Collapse in={this.state.mailexpanded} timeout="auto" unmountOnExit>
        <Grid container>
            <Grid item xs={12} sm={12} md={12}>
                <form className={classes.container} >
                    <TextField
                        label="Address Line 1"
                        id="simple-start-adornment"
                        name="permanentAddress"
                        className={classes.select}
                        value={clientInfo.permanentAddress}
                        onChange={this.fieldOnChange.bind(this)}
                    />
                    <TextField
                        select
                        label="Municipality"
                        className={classes.select}
                        name="permanentMunicipalityCode"
                        value={clientInfo.permanentMunicipalityCode}
                        onChange={this.fieldOnChange.bind(this)}
                    >
                        {this.state.municipalityList.data.map(option => (
                            <MenuItem key={option.code} value={option.code + "|" + option.name}>
                                {option.name}
                            </MenuItem>
                        ))}
                    </TextField>
                    <TextField
                        select
                        label="Province"
                        className={classes.select}
                        name="permanentProvince"
                        value={clientInfo.permanentProvince}
                        onChange={this.fieldOnChange.bind(this)}
                    >
                        {this.state.provinceList.data.map(option => (
                            <MenuItem key={option.code} value={option.code + "|" + option.name}>
                                {option.name}
                            </MenuItem>
                        ))}
                    </TextField>

                    <TextField
                        select
                        label="Country"
                        className={classes.select}
                        name="permanentCountry"
                        value={clientInfo.permanentCountry}
                        onChange={this.fieldOnChange.bind(this)}
                    >
                        {this.state.countriesList.data.map(option => (
                            <MenuItem key={option.alpha2} value={option.alpha2 + "|" + option.name}>
                                {option.name}
                            </MenuItem>
                        ))}
                    </TextField>
                    <TextField
                        label="Zipcode"
                        id="simple-start-adornment"
                        name="permanentZipCode"
                        className={classes.select}
                        value={clientInfo.permanentZipCode}
                        onChange={this.fieldOnChange.bind(this)}
                    />
                </form>
            </Grid>
        </Grid>
      </Collapse>
     </CardBody>
                    </Card>
                </GridItem>
            )
        }
    }

    
  handleExpandClick() {
    return e => {
        e.preventDefault()
        if (!this.state.mailexpanded) {
    this.setState({ mailexpanded : true });
        } else {
            this.setState({ mailexpanded : false });

        }
  };
  }

    handleChange = e => {
        console.log("typing", e.currentTarget.value);
        this.setState({ text: e.currentTarget.value });
    };


    fieldOnChange = sender => {
        let fieldName = sender.target.name;
        let value = sender.target.value;
        let state = this.state;
        this.setState({
            ...state,
            clientInfo: { ...state.clientInfo, [fieldName]: value }
        });
    }


    selectOnChange(fieldname, value) {
        let vfieldName = fieldname;
        //let value = sender.target.value;
        let state = this.state;
        this.setState({
            ...state,
            clientInfo: { ...state.clientInfo, [vfieldName]: value }
        });
        console.log(this.state.clientInfo)
    }

    handleModalHide() {
        return () => {
            let { items } = this.state
            items = items.map(item => ({
                ...item,
                showModal: false,
            }))
            this.setState({ items })
        }
    }

    handleModalShow() {
        return e => {
            e.preventDefault();

            this.setState({ showModal: true });
        };
    }


    handleEditItem(selectedItem) {
        return e => {
            e.preventDefault()
            let { items } = selectedItem// this.state
            items = selectedItem.map(item => ({
                ...item,
                showModal: selectedItem.id === item.id
            }));
            this.setState({ items });
        };
    };

    docOnChange = sender => {
        let fieldName = sender.target.name;
        let value = sender.target.value;
        let state = this.state;
        this.setState({ vDocType: value });
    };

    getDedup(iCode) {
        console.log(iCode);
        let dedupResults = [];
        let icdbResults = [];
        let carsResults = [];
        axios.get('/json/acceptancecriteria.json')
            .then(response => {
                let results1 = null;

                if (typeof (response) === "string") {
                    results1 = JSON.parse(response.data);
                } else {
                    results1 = response.data;
                }
                //this.setState({acResult: results1.data.dedup});

                if (!iCode.dedupResult) {
                    dedupResults.push(results1.dedup.filter(item => item.code === "0"));
                } else {
                    dedupResults.push(results1.dedup.filter(item => item.code === iCode.dedupResult));
                }

                if (!iCode.icdbResult) {
                    icdbResults.push(results1.icdb.filter(item => item.code === "0"));
                } else {
                    icdbResults.push(results1.icdb.filter(item => item.code === iCode.icdbResult));
                }

                if (!iCode.updatedCARSScore) {
                    carsResults.push(results1.cars.filter(item => item.code === "0"));
                } else {
                    carsResults.push(results1.cars.filter(item => item.code === iCode.updatedCARSScore));
                };

                this.setState({ dedupDesc: dedupResults });
                this.setState({ icdbDesc: icdbResults });
                this.setState({ carsDesc: carsResults });


            })
            .catch((ex) => {
                console.log('dedupError: ' + ex);
            });
    }

    removeBar(text) {
        try {
            let str = (text);
            return (str.substring(str.indexOf('|') + 1));
        }
        catch (e) {
            return null;
        }
    };

    render() {
        const { classes } = this.props;

        const { clientInfo, fields, cameraOpen } = this.state;
        let wname = clientInfo.firstName + " " + clientInfo.middleName + " " + clientInfo.lastName;
        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric' }
        console.log(this.state)

        if (!clientInfo.referenceNumber) {
            return (
                <div>
                    <Grid container
                        spacing={0}
                        justify="center"
                        alignItems="center"
                        style={{ minHeight: '100vh' }} >
                        <Grid item xs={8} justify="center">
                            <LinearProgress />
                        </Grid>
                    </Grid>
                </div>
            )
        }
        return (
            <div>
                <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                    <br />
                        <Card>
                            <CardHeader color="plain">
                                <h5 className={classes.cardTitleBlue}>APP. REF NO.: {clientInfo.referenceNumber}</h5>
                            </CardHeader>
                            <CardBody>
                                <GridContainer>
                                    <GridItem xs={12} sm={12} md={6}>

                                        <Card>
                                            <CardBody>
                                                <h6 className={classes.cardTitleBlack}>ACCEPTANCE CRITERIA:</h6>
                                                <Grid container alignItems="center">
                                                    <Grid item xs={12} sm={12} md={4} ><Primary>ICDB Result: </Primary>

                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={7}>
                                                        {this.state.icdbDesc.map(item =>
                                                            <NACModal btnCap={item[0].desc} item={item} btnDisabled = {false} />
                                                            //  <Buttonx className={classes.txtbutton} onClick={this.handleEditItem(item)}  >{item[0].desc}</Buttonx>
                                                        )}
                                                    </Grid>
                                                </Grid>
                                                <Grid container alignItems="center">
                                                    <Grid item xs={12} sm={12} md={4}><Primary>CARS Rating: </Primary>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={7} >
                                                        {this.state.carsDesc.map(item =>
                                                            <NACModal btnCap={item[0].desc} item={item} btnDisabled = {false}  />
                                                            //  <Buttonx className={classes.txtbutton} onClick={this.handleEditItem(item)}  >{item[0].desc}</Buttonx>
                                                        )}
                                                    </Grid>
                                                </Grid>
                                                <Grid container alignItems="center">
                                                    <Grid item xs={12} sm={12} md={4}><Primary>Dedup: </Primary>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={7} >
                                                        {this.state.dedupDesc.map(item =>
                                                            <NACModal btnCap={item[0].desc} item={item} btnDisabled = {false}  />
                                                            //  <Buttonx className={classes.txtbutton} onClick={this.handleEditItem(item)}  >{item[0].desc}</Buttonx>
                                                        )}
                                                    </Grid>
                                                </Grid>
                                            </CardBody>
                                        </Card>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={6}>
                                        <Card>
                                            <CardBody>
                                                <h6 className={classes.cardTitleBlack}>ACCOUNT INFORMATION:</h6>

                                                <Grid container alignItems="center">
                                                    <Grid item xs={12} sm={12} md={4}><Primary>Product Type: </Primary>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={7}><InputBase name="productType" value={clientInfo.productType} className={classes.input} inputProps={{ 'aria-label': 'naked' }} onChange={this.fieldOnChange.bind(this)} />
                                                    </Grid>
                                                </Grid>
                                                <Grid container alignItems="center">
                                                    <Grid item xs={12} sm={12} md={4}><Primary>RC Code: </Primary>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={7}><InputBase name="branchCode" value={clientInfo.branchCode} className={classes.input} inputProps={{ 'aria-label': 'naked' }} onChange={this.fieldOnChange.bind(this)} />
                                                    </Grid>
                                                </Grid>
                                            </CardBody>
                                        </Card>
                                    </GridItem>
                                </GridContainer>

                                <GridContainer>
                                    <GridItem xs={12} sm={12} md={6}>

                                        <Card>
                                            <CardHeader color="gray">
                                                <h6 className={classes.cardTitleBlack}>Personal Information:</h6>
                                            </CardHeader>
                                            <CardBody>
                                                <Grid container alignItems="center">
                                                    <Grid item xs={12} sm={12} md={4}><Primary>Full Name: </Primary>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={7}>
                                                        <InputBase id="fullname" value={wname} className={classes.input} inputProps={{ 'aria-label': 'naked' }} />
                                                    </Grid>
                                                </Grid>
                                                <Grid container alignItems="center">
                                                    <Grid item xs={12} sm={12} md={4}><Primary>Birth Place: </Primary>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={7} >
                                                        {this.renderBirthPlace()}
                                                    </Grid>
                                                </Grid>
                                                <Grid container alignItems="center">
                                                    <Grid item xs={12} sm={12} md={4}><Primary>Birth Date: </Primary>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={7} >
                                                        <InputBase name="birthDate" required data-provide="datepicker" value={moment(clientInfo.birthDate).format("LL")} className={classes.input} inputProps={{ 'aria-label': 'naked' }} onChange={this.fieldOnChange.bind(this)} />
                                                    </Grid>
                                                </Grid>
                                                <Grid container alignItems="center">
                                                    <Grid item xs={12} sm={12} md={4}><Primary>Citizenship: </Primary>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={7} >

                                                        <form className={classes.container} >
                                                            <TextField
                                                                select
                                                                className={classes.select}
                                                                name="nationality"
                                                                value={clientInfo.nationality}
                                                                onChange={this.fieldOnChange.bind(this)}
                                                            >
                                                                {this.state.ranges.map(option => (
                                                                    <MenuItem key={option.value} value={option.value + "|" + option.label}>
                                                                        {option.label}
                                                                    </MenuItem>
                                                                ))}
                                                            </TextField>
                                                        </form>
                                                    </Grid>
                                                </Grid>
                                                <Grid container alignItems="center">
                                                    <Grid item xs={12} sm={12} md={4}><Primary>US Person: </Primary>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={7} >
                                                        <InputBase id="" defaultValue="No" className={classes.input} inputProps={{ 'aria-label': 'naked' }} />
                                                    </Grid>
                                                </Grid>
                                            </CardBody>
                                        </Card>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={6}>
                                        <Card>
                                            <CardHeader color="gray">
                                                <h6 className={classes.cardTitleBlack}>Contact Details:</h6>

                                            </CardHeader>
                                            <CardBody>

                                                <Grid container alignItems="center">
                                                    <Grid item xs={12} sm={12} md={4}><Primary>Mobile Number: </Primary>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={7}><InputBase name="mobileNumber" value={clientInfo.mobileNumber} className={classes.txtbutton} inputProps={{ 'aria-label': 'naked' }} onChange={this.fieldOnChange.bind(this)} />
                                                    </Grid>
                                                </Grid>
                                                <Grid container alignItems="center">
                                                    <Grid item xs={12} sm={12} md={4}><Primary>Email: </Primary>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={7}><InputBase name="emailAddress" value={clientInfo.emailAddress} className={classes.txtbutton} inputProps={{ 'aria-label': 'naked' }} onChange={this.fieldOnChange.bind(this)} />
                                                    </Grid>
                                                </Grid>
                                            </CardBody>
                                        </Card>
                                    </GridItem>

                                    {this.renderAddress()}
                                    <GridItem xs={12} sm={12} md={6}>
                                        <Card>
                                            <CardHeader color="gray">
                                                <h6 className={classes.cardTitleBlack}>Financial Information:</h6>
                                            </CardHeader>
                                            {this.renderFinancial()}

                                        </Card>
                                    </GridItem>


                                    <GridItem xs={12} sm={12} md={6}>
                                        <Card>
                                            <CardHeader color="gray">
                                                <h6 className={classes.cardTitleBlack}>Identification Documents:</h6>

                                            </CardHeader>
                                            <CardBody>

                                                <Grid container alignItems="center">
                                                    <Grid item xs={12} sm={12} md={4}><Primary >Valid ID: </Primary>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={7}>

                                                        <Select
                                                            value={this.state.vDocType}
                                                            onChange={this.docOnChange}
                                                            input={<OutlinedInput labelWidth={this.state.labelWidth} name="age" id="outlined-age-simple" />}
                                                            className={classes.boxedselect}
                                                        >
                                                            <MenuItem value={"01"}>SSS UMID</MenuItem>
                                                            <MenuItem value={"02"}>Driver's License</MenuItem>
                                                            <MenuItem value={"03"}>Passport</MenuItem>
                                                        </Select>
                                                    </Grid>
                                                </Grid>

                                                <Grid container alignItems="center">
                                                    <Grid item xs={12} sm={12} md={4}>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={7}>
                                                        <CameraModal docType={this.state.vDocType} imgType="clientID" />
                                                    </Grid>
                                                </Grid>

                                                <Grid container alignItems="center">
                                                    <Grid item xs={12} sm={12} md={4}><Primary>Personal Photo: </Primary>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={7}> <CameraModal docType="0059" imgType="clientPhoto" />
                                                    </Grid>
                                                </Grid>
                                            </CardBody>
                                        </Card>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={12}>
                                        <Card>
                                            <CardHeader color="gray">
                                                <h6 className={classes.cardTitleBlack}>Remarks:</h6>

                                            </CardHeader>
                                            <CardBody>
                                                <Grid container alignItems="center" >
                                                    <Grid item xs={12} sm={12} md={12}>

                                                        <TextField
                                                            id="remarks"
                                                            style={{ margin: 8 }}
                                                            placeholder="NAC Remarks"
                                                            fullWidth
                                                            multiline
                                                            rowsMax="4"
                                                            margin="normal"
                                                            variant="outlined"
                                                            InputLabelProps={{
                                                                shrink: true,
                                                            }}
                                                            onChange={this.fieldOnChange.bind(this)}
                                                            value={clientInfo.remarks}
                                                        />
                                                    </Grid>
                                                </Grid>
                                            </CardBody>
                                        </Card>
                                    </GridItem>

                                </GridContainer>
                            </CardBody>
                            <CardFooter>
                                <Grid container direction="row"
                                    justify="center" spacing={8}
                                    alignItems="center" xs={12} sm={12} md={12} >

                                    <RouterLink to={{ pathname: "/admin/nacbranchconfirm", state: this.state.clientInfo }}>
                                        <Button variant="contained" color="info" block={true} >
                                            For Client's Confirmation          </Button>
                                    </RouterLink>

                                    <NACSubmitted submitInfo={this.state.clientInfo} referenceNumber={clientInfo.referenceNumber} isConfirmed={this.state.isConfirmed} />

                                    <Button variant="contained" color="danger" block={true} disabled ={!this.state.isConfirmed}>
                                        For Compliance    </Button>
                                      
                                </Grid>
                                
                            </CardFooter>
                            <br />
                        </Card>
                    </GridItem>
                </GridContainer>
                {/* 
                {this.state.items.map((item, index) => (
                        <ItemModal
                            key={item.code}
                            show={item.showModal}
                            onHide={this.handleModalHide()}
                            onItemChange={this.handleItemChange}
                            item={item}
                        />

                    ),
                        console.log(this.state.items))} */}
            </div>
        );
    }
}


NACBranch.propTypes = {
    classes: PropTypes.object.isRequired
};
export default withStyles(nacStyle)(NACBranch);