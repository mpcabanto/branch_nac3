import React from "react";
import axios from 'axios';
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
import nacStyle from "assets/jss/material-dashboard-react/views/nacStyle.jsx";
import moment from 'moment';


import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import Grid from "@material-ui/core/Grid";
import LinearProgress from '@material-ui/core/LinearProgress';

import {  Link as RouterLink } from "react-router-dom";
import Link from '@material-ui/core/Link';
import {
    grayColor
} from "assets/jss/material-dashboard-react.jsx";

const AdapterLink = React.forwardRef((props, ref) => <RouterLink innerRef={ref} {...props} />);


class NACBranchConfirm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: "",
            isOpen: false,
            clientInfo: {},
            vclientPhoto: "",
            open: false,
            setOpen: false,
            vclientID: "",
            isError: false
        };
        this.uploadForm = this.uploadForm.bind(this);
        this.confirmed = this.confirmed.bind(this);
    };

    handleClickOpen = () => {
        this.setState({ setOpen: true });
    };
    handleClose = () => {
        this.setState({ setOpen: false });
    };

    async  componentDidMount() {
        await this.setState({ submitInfo: this.props.location.state });
        try {
            if (!this.state.vresult && !this.state.isError) {
                this.uploadForm();
            }
        } catch (e) {
            this.setState({ errorlog: "DocumentImage error" });
        }
    }

    confirmed = async () => {

        const { submitInfo } = this.state;
        const headers = { "headers": "Access-Control-Allow-Origin" }
        let rc = localStorage.getItem("groups");
        let vappno = this.state.vRefNo;
        axios.post('http://localhost:11352/api/WebApi/GetKafka', { kafkaurl: "https://10.11.28.29:8243/onboarding/applications?referenceNumber=" + submitInfo.referenceNumber, kafkadata: null })
            //      axios.post(''https://10.11.27.104:3001/api/WebApi/GetKafka', { kafkaurl: "https://10.11.28.29:8243/onboarding/applications?referenceNumber=" + submitInfo.referenceNumber, kafkadata: null })
            //  axios.post('https://10.11.27.104:3001/api/WebApi/GetKafka', { kafkaurl: "http://10.11.27.104:5003/api/NacReview/3/" + rc , kafkadata: null })
            // await axios.get('/json/customer3.json')
            .then(response => {
                let results = null;
                if (typeof (response.data) === "string") {
                    results = JSON.parse(response.data);
                } else {
                    results = response.data;
                }
                this.setState({ responseInfo: results });
                //console.log(this.state.errorResult);

                if (this.state.responseInfo.statusCode = 50) {

                    let state = this.state;
                    this.setState({
                        ...state,
                        submitInfo: { ...state.submitInfo, statusCode: this.state.responseInfo.statusCode }
                    });

                    this.props.history.push({ pathname: "/admin/nacbranch", state: this.state.clientInfo })
                } else {
                    alert("Application is not yet confirmed");

                }
            })
            .catch((ex) => {
                console.error(ex);
                this.setState({ isError: true });
                alert(ex);
            });
    }

    uploadForm = async () => {

        const { submitInfo } = this.state;
        console.log(submitInfo);
        const config = {
            headers: {
                // "Access-Control-Allow-Origin": "*",
                // "Content-Type": "application/json",
                "Content-Type": "application/x-www-form-urlencoded",
                "Accept": "application/json"
            }
        };

        const form1 = {
            id: localStorage.getItem("userId"),
            lastName: submitInfo.lastName,
            gender: submitInfo.gender,
            documentType: "",
            mobileNumber: submitInfo.mobileNumber,
            referenceNumber: submitInfo.referenceNumber,
            civilStatus: submitInfo.civilStatus,
            type: "I",
            presentAddress: {
                zipCode: submitInfo.presentZipCode,
                country: submitInfo.presentCountry,
                province: submitInfo.presentProvince,
                city: submitInfo.presentMunicipalityCode,
                address: submitInfo.presentAddress
            },
            permanentAddress: {
                zipCode: submitInfo.permanentZipCode,
                country: submitInfo.permanentCountry,
                province: submitInfo.permanentProvince,
                city: submitInfo.permanentMunicipalityCode,
                address: submitInfo.permanentAddress
            },
            mailingAddress: {
                zipCode: submitInfo.zipcodeperm,
                country: submitInfo.countryperm,
                province: submitInfo.provinceperm,
                city: submitInfo.cityperm,
                address: submitInfo.streetperm
            },
            incomeSource: submitInfo.sourceofIncome,
            //incomeSource:"SF1",
            emailAddress: submitInfo.emailAddress,
            productType: submitInfo.productType,
            monthlyIncome: "",
            birthPlace: submitInfo.placeofBirth,
            profession: submitInfo.natureofWork,
            //profession: "REG",
            requestor: "TESTUSER",
            birthDate: submitInfo.birthDate,
            firstName: submitInfo.firstName,
            nationality: submitInfo.nationality,
            //nationality: "FN1",
            responsibilityCode: "111",
            middleName: submitInfo.middleName,
            spouseName: submitInfo.spouseName,
            fatca: "NO",
            accountFacility: ""
        };

        console.log(form1);

        await axios.post('https://10.11.27.104:3001/api/WebApi/PostKafka', { kafkaurl: "https:/psbapiadev:8243/onboarding/applications/update", kafkadata: form1 })
            .then(response => {
                console.log(response)
                this.setState({ vresult: response.data });
                this.setState({ success: true, loading: false });

            })
            .catch((ex) => {
                console.error(ex);
                this.setState({ success: true, loading: false });

                this.setState({ isError: true });


            });

    }//end uplodForm


    render() {
        const { classes } = this.props;

        if (!this.state.vresult && !this.state.isError) {
            return (
                <div>
                    <Grid container
                        spacing={0}
                        justify="center"
                        alignItems="center"
                        style={{ minHeight: '100vh' }} >
                        <Grid item xs={8} justify="center">
                            <LinearProgress />
                        </Grid>
                    </Grid>
                </div>
            )
        }

        if (this.state.isError) {
            return (
                <div>
                    <Grid container
                        spacing={0}
                        justify="center"
                        alignItems="center"
                        style={{ minHeight: '80vh' }} >
                        <Grid item xs={8} justify="center">

                            <Card>
                                <CardHeader color="plain" >
                                    <h6 className={classes.cardTitleBlue}>      Failed to submit for client's confirmation.</h6>
                                </CardHeader>


                                <CardFooter >
                                    <Grid container direction="row" alignItems="center" justify="space-evenly" >

                                        <Button variant="contained" color="blueGradient" block={true} onClick={this.uploadForm.bind()}  >
                                          
                                                Re-submit?
                                        </Button>
                                    </Grid>
                                    <Grid container direction="row" >
                                    <Link component={AdapterLink} to={{ pathname: "/admin/nacbranch", state: this.state.submitInfo }} >
                                    <Button variant="contained" color="redGradient" block={true}   >
                                                Cancel
                                        </Button>
                                        </Link>
                                    </Grid>
                                </CardFooter>
                            </Card>
                        </Grid>
                    </Grid>
                </div>
            )
        }
        return (
            <div>
                <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardHeader color="plain">
                                <h5 className={classes.cardTitleBlue}>APP. REF NO.:  {this.state.submitInfo.referenceNumber} </h5>
                            </CardHeader>
                            <CardBody>

                            </CardBody>
                            <CardFooter>
                                <Grid container direction="row"
                                    justify="center" spacing={8}
                                    alignItems="center" xs={12} sm={12} md={12} >
                                    <Button variant="contained" color="info" onClick={this.confirmed.bind()} >
                                        Confirm
                                         </Button>
                                    <Link component={AdapterLink} to={{ pathname: "/admin/nacbranch", state: this.state.submitInfo }} >
                                        <Button variant="contained" color="info" >
                                            Clear       </Button>
                                    </Link>
                                </Grid>
                            </CardFooter>
                        </Card>
                    </GridItem>
                </GridContainer>


            </div>
        );
    }
}


NACBranchConfirm.propTypes = {
    classes: PropTypes.object.isRequired
};
export default withStyles(nacStyle)(NACBranchConfirm);