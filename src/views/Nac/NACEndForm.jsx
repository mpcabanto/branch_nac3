import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
import nacStyle from "assets/jss/material-dashboard-react/views/nacStyle.jsx";

import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import Grid from "@material-ui/core/Grid";


import { Link as RouterLink } from "react-router-dom";
import Link from '@material-ui/core/Link';
const AdapterLink = React.forwardRef((props, ref) => <RouterLink innerRef={ref} {...props} />);

class NACEndForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: "",
            isOpen: false
        };
    };

    


    render() {
        const { classes } = this.props;

        return (
            <div>
                <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardBody >
                                
                                <Grid container direction="row"
                                    justify="center" spacing={2}
                                    alignItems="center" xs={12} sm={12} md={12} >
                                <h6 className={classes.centertextblue}>Application Submitted.</h6>
                                </Grid>
                            </CardBody>
                            <CardFooter>
                                <Grid container direction="row"
                                    justify="center" spacing={2}
                                    alignItems="center" xs={12} sm={12} md={12} >
                                    <Link component={AdapterLink} to="/admin/nacmain">
                                        <Button variant="contained" color="info" >
                                            Go back to Main Page       </Button>
                                    </Link>
                                </Grid>
                            </CardFooter>
                        </Card>
                    </GridItem>
                </GridContainer>

            </div>
        );
    }
}


NACEndForm.propTypes = {
    classes: PropTypes.object.isRequired
};
export default withStyles(nacStyle)(NACEndForm);