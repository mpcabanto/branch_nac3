import React from "react";
import axios from 'axios';
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
import nacStyle from "assets/jss/material-dashboard-react/views/nacStyle.jsx";

import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";

import Grid from "@material-ui/core/Grid";
import CardMedia from '@material-ui/core/CardMedia';
import SnackbarContent from "components/Snackbar/SnackbarContent.jsx";
import Snackbar from "components/Snackbar/Snackbar.jsx";
import AddAlert from "@material-ui/icons/AddAlert";
import CircularProgress from '@material-ui/core/CircularProgress';

import NacList from "views/Nac/NacList.jsx";
import avatar from "assets/img/faces/Client.JPG";

import { Route, NavLink, Link, withRouter, Redirect } from "react-router-dom";
//import Link from '@material-ui/core/Link';
//import NACEndForm from "views/Nac/NacList.jsx";
import ReactDOM from 'react-dom';
//const AdapterLink = React.forwardRef((props, ref) => <RouterLink innerRef={ref} {...props} />);

class NACSubmitted extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: "",
            isOpen: false,
            clientInfo: {}, vresult: {},
            bc: false,
            loading: false, success: false, 
            colorcode: "danger",
            imageArr: []
        };
    };

    componentWillUnmount() {
        var id = window.setTimeout(null, 0);
        while (id--) {
            window.clearTimeout(id);
        }
    }

    showNotification(place) {
        var x = [];
        x[place] = true;
        this.setState(x);
        this.alertTimeout = setTimeout(
            function () {
                x[place] = false;
                this.setState(x);
            }.bind(this),
            12000
        );
    }

    uploadForm = (event) => {
        event.preventDefault();
        
        if (!this.state.loading) {
                  
            this.setState({ success: false, loading:true });
          };

        const { referenceNumber, submitInfo } = this.props;
        console.log(submitInfo);
        const { clientInfo } = this.state;
        const config = {
            headers: {
                // "Access-Control-Allow-Origin": "*",
                // "Content-Type": "application/json",
                "Content-Type": "application/x-www-form-urlencoded",
                "Accept": "application/json"
            }
        };

if ( sessionStorage.getItem("clientID") ) {
 this.state.imageArr.push(JSON.parse(sessionStorage.getItem("clientID")) );
   console.log(this.state.imageArr);
};
if ( sessionStorage.getItem("clientPhoto") ) {
 this.state.imageArr.push(JSON.parse(sessionStorage.getItem("clientPhoto")) );
   console.log(this.state.imageArr);
};
        const form1 = {
            "dedupDetails": submitInfo.dedupDetails,
             "icdbDetails": submitInfo.icdbDetails,
             "carsDetails": submitInfo.carsDetails,
             "partition": submitInfo.partition,
            "offset": submitInfo.offset,
            "statusCode": "Ok",
            "statusDescription": null,
            "referenceNumber": referenceNumber,
            "existingLevel": submitInfo.existingLevel,
            "applicationLevel": submitInfo.applicationLevel,
            "l1ApplicationDate": submitInfo.l1ApplicationDate,
            "l1Status": submitInfo.l1Status,
            "l2ApplicationDate": submitInfo.l2ApplicationDate,
            "l2Status": submitInfo.l2Status,
            "l3ApplicationDate": submitInfo.l3ApplicationDate,
            "l3Status": submitInfo.l3Status,
            "lastName": submitInfo.lastName,
            "middleName": submitInfo.middleName,
            "firstName": submitInfo.firstName,
            "birthDate": submitInfo.birthDate,
            "mobileNumber": submitInfo.mobileNumber,
            "emailAddress": submitInfo.emailAddress,            
	"presentAddress": submitInfo.presentAddress,
	"presentMunicipalityCode": submitInfo.presentMunicipalityCode,
	"presentProvince": submitInfo.presentProvince,
	"presentCountry": submitInfo.presentCountry,
	"presentZipCode": submitInfo.presentZipCode,
	"permanentAddress": submitInfo.permanentAddress,
	"permanentMunicipalityCode": submitInfo.permanentMunicipalityCode,
	"permanentProvince": submitInfo.permanentProvince,
	"permanentCountry": submitInfo.permanentCountry,
	"permanentZipCode": submitInfo.permanentZipCode,
            "municipalityCode": submitInfo.municipalityCode,
            "sourceofIncome": submitInfo.sourceofIncome,
            "natureofWork": submitInfo.natureofWork,
            "placeofBirth": submitInfo.placeofBirth,
            "nationality": submitInfo.nationality,
            "productType": submitInfo.productType,
            "dedupResult": submitInfo.dedupResult,
            "icdbResult": submitInfo.icdbResult,
            "carsScore": submitInfo.carsScore,
            "carsRiskProfile": submitInfo.carsRiskProfile,
            "carsOverallRiskRating": submitInfo.carsOverallRiskRating,
            "dedupDisposition": submitInfo.dedupDisposition,
            "updatedCARSScore": submitInfo.updatedCARSScore,
            "updatedCARSRiskProfile": submitInfo.updatedCARSRiskProfile,
            "updatedCARSOverallRiskRating": submitInfo.updatedCARSOverallRiskRating,
            "ciF_ID": submitInfo.ciF_ID,
            "accountNumber": submitInfo.accountNumber,
            "videoCallSchedule": submitInfo.videoCallSchedule,
            "remarks": submitInfo.remarks,
            "imageDetails": this.state.imageArr,
            "branchCode": submitInfo.branchCode,            
            "civilStatus": submitInfo.civilstatus,
            "sourceApplication": submitInfo.sourceApplication
        }
            ;
        console.log(form1);

        axios.post('https://10.11.27.104:3001/api/WebApi/PostKafka', { kafkaurl: "http://10.11.27.104:5003/api/NacReview", kafkadata: form1 })
            .then(response => {
                console.log(response)
                this.setState({ vresult: response.data });
                this.setState({ success: true, loading:false });
        
                if (this.state.vresult.statusCode === "OK") {
                    this.setState({ colorcode: "info" });
                    this.showNotification("bc");
                    // this.props.history.pushState({ pathname: '/admin/endform', state: this.state.fields });
                } else {
                    //console.log(this.state.vresult.statusCode + ": " + this.state.vresult.statusDescription);                    
                    this.setState({ colorcode: "danger" });
                    this.showNotification("bc");
                }
            })
            .catch((ex) => {
                console.error(ex);
                this.setState({ success: true, loading:false });
        
                this.setState({ isError: true });
                this.showNotification("bc");
                
              
            });

    }//end uplodForm

    render() {
        if (this.state.vresult.statusCode === "OK") {
            return (
                <Redirect to='/admin/endform' />
            )
        }
        if (this.state.bc) {
            return (
                <div>
                    <GridContainer>
                        <GridItem xs={12} sm={12} md={12}>
                            <Card>
                                <CardBody >
                                    <Grid container direction="row"
                                        justify="center"
                                        alignItems="center" xs={12} sm={12} md={12} >
                                        <Snackbar
                                            place="bc"
                                            color={this.state.colorcode}
                                            icon={AddAlert}
                                            message={this.state.vresult.statusCode + ": " + this.state.vresult.statusDescription}
                                            open={this.state.bc}
                                            closeNotification={() => this.setState({ bc: false })}
                                            close
                                        />
                                    </Grid>
                                </CardBody>
                            </Card>
                        </GridItem>
                    </GridContainer>

                </div>
            )
        }
        const { classes, isConfirmed } = this.props;

        return (
            <div>
            <Button variant="contained" color="info" onClick={this.uploadForm.bind(this)} block={true} disabled={ this.state.loading || !isConfirmed }
                    >   For Approval                    
            {this.state.loading && <CircularProgress size={24} className={classes.buttonProgress} />}
                    </Button>
            </div>
        );
    }
}


NACSubmitted.propTypes = {
    classes: PropTypes.object.isRequired,
    submitInfo: PropTypes.object,
    referenceNumber: PropTypes.string.isRequired,
    isConfirmed: PropTypes.bool
};
export default withStyles(nacStyle)(NACSubmitted);