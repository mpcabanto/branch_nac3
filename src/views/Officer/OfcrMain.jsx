
import React from "react";
import PropTypes from "prop-types";
import { Route, NavLink, Link as RouterLink } from "react-router-dom";
import ComboBox from '@zippytech/react-toolkit/ComboBox'
import '@zippytech/react-toolkit/ComboBox/index.css'

// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Store from "@material-ui/icons/Store";
import Warning from "@material-ui/icons/Warning";
import DateRange from "@material-ui/icons/DateRange";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import Accessibility from "@material-ui/icons/Accessibility";
import BugReport from "@material-ui/icons/BugReport";
import Code from "@material-ui/icons/Code";
import Cloud from "@material-ui/icons/Cloud";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Table from "components/Table/Table.jsx";
import Tasks from "components/Tasks/Tasks.jsx";
import CustomTabs from "components/CustomTabs/CustomTabs.jsx";
import Danger from "components/Typography/Danger.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import Info from "components/Typography/Info.jsx";
import Link from '@material-ui/core/Link';
import { Button, Paper, Typography } from "@material-ui/core";

import { bugs, website, server } from "variables/general.jsx";



import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts.jsx";

import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";

const AdapterLink = React.forwardRef((props, ref) => <RouterLink innerRef={ref} {...props} />);

class OfcrMain extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      suggestions: [],
      value: "",
      logKey: 0,
      logEntries: "",
      presentCountry: null,
      countries: [
        {
          "name": "United States",
          "code": "US"
         },
      {
          "name": "United Kingdom",
          "code": "GB"
        },
      {
          "name": "Canada",
          "code": "CA"
         },
      {
          "name": "Afghanistan",
          "code": "AF"
         },
      {
          "name": "Aland Islands",
          "code": "AX"
      }
    ]
    };
  }

   
  
  render() {
    const { classes } = this.props;
    const { suggestions, value, logEntries, countries } = this.state;
    const dataSource = countries.map(country => ({
      id: country.code,
      label: country.name
    }))
    console.log(this.state)
    return (
      <div>
      <div style={{ marginBottom: 20 }}>
        You can type-in to choose a country from the list.
      </div>
      Choose a country:
       <ComboBox
      style={{ width: 300 }}
      dataSource={dataSource}
      allowCustomTagCreation={true}
      multiple={false}
      autocomplete={true}
      clearIcon={true}
      toggleIcon={false}
     // searchable={true}
      toggleExpandOnClick={false}
      expandOnFocus={false}
      expandOnClick={false}
      collapseOnSelect={true}
   //   lazyDataSource
      value={this.state.presentCountry}
      defaultValue={this.state.presentCountry}
      onChange={value => {
        this.setState({
          presentCountry: value
        });
      }}
      newCustomTagText={({ text }) => <strong>create new country: {text} </strong>}
    />
  
    </div>

    );
  }
}

OfcrMain.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(OfcrMain);
